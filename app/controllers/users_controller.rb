class UsersController < ApplicationController
	def new
		@user = User.new
	end
	def create
		@user = User.new(user_params)
		@user.email.downcase!

		# difference between flash[:notice] and flash.now[:notice]
		# -----
		# "Flash[:notice]‘s message will persist to the next
		# action and should be used when redirecting to
		# another action via the ‘redirect_to’ method.
		# -----
		# Flash.now[:notice]‘s message will be displayed in
		# the view your are rendering via the ‘render’
		# method."
		
		if @user.save
			flash[:notice] = "Account created"
			redirect_to root_path
		else
			flash.now.alert = "Could not create account"\
				"Make sure everything is correct"
			render :new
		end
	end

	private

	def user_params
		params.require(:user).permit(:name, :email, :password, :password_confirmation)
	end

end
